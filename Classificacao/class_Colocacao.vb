﻿Imports Classificacao

Public Class class_Colocacao


    Public Sub encerrarRodada()
        Try
            encerrarJogos()

            Dim times = From time In _Times Select time._Nome_Time Distinct

            Dim pontos As class_Ranking

            For Each time In times

                pontos = New class_Ranking

                pontos._Nome_Time = time

                For Each jogo In jogos

                    If time = jogo._Nome_Time_Casa Then

                        pontos = AdicionaPontuacao(pontos, jogo._Vitoria_Casa, jogo._Derrota_Casa, jogo._Empate_Casa, jogo._Gols_Casa, jogo._Pontos_Casa)

                    ElseIf time = jogo._Nome_Time_Visitante Then

                        pontos = AdicionaPontuacao(pontos, jogo._Vitoria_Visitante, jogo._Derrota_Visitante, jogo._Empate_Visitante, jogo._Gols_Visitante, jogo._Pontos_Visitante)

                    End If

                Next

                Ranking.Add(pontos)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function AdicionaPontuacao(pontos As class_Ranking, vitoria As Integer, derrota As Integer, empate As Integer, gols As Integer, pontosRodada As Integer) As class_Ranking
        pontos._Vitoria += vitoria
        pontos._Derrota += derrota
        pontos._Empate += empate
        pontos._Gols += gols
        pontos._Pontos += pontosRodada
        Return pontos
    End Function

    Private Ranking As New List(Of class_Ranking)
    Public Property _Ranking() As List(Of class_Ranking)

        Get
            Return Ranking
        End Get
        Set(ByVal value As List(Of class_Ranking))
            Ranking = value
        End Set
    End Property


    Private jogos As New List(Of class_Jogo)
    Public Property _Jogos() As List(Of class_Jogo)
        Get
            Return jogos
        End Get
        Set(ByVal value As List(Of class_Jogo))
            jogos = value
        End Set
    End Property

    Private times As New List(Of class_Time)
    Public Property _Times() As List(Of class_Time)
        Get
            Return times
        End Get
        Set(ByVal value As List(Of class_Time))
            times = value
        End Set
    End Property

    Private Sub encerrarJogos()

        For Each jogo In jogos
            adicionaTime(jogo)
            If jogo._Gols_Casa > jogo._Gols_Visitante Then
                jogo._Vitoria_Casa = 1
                jogo._Derrota_Visitante = 1
            ElseIf jogo._Gols_Casa = jogo._Gols_Visitante Then

                jogo._Empate_Casa = 1
                jogo._Empate_Visitante = 1
            Else
                jogo._Derrota_Casa = 1
                jogo._Vitoria_Visitante = 1
            End If
            jogo._Pontos_Casa = IIf(jogo._Vitoria_Casa = 1, 3, jogo._Vitoria_Casa) + jogo._Empate_Casa
            jogo._Pontos_Visitante = IIf(jogo._Pontos_Visitante = 1, 3, jogo._Pontos_Visitante) + jogo._Empate_Visitante
        Next


    End Sub

    Private Sub adicionaTime(jogo As class_Jogo)
        Dim Time As class_Time

        Time = New class_Time
        Time._Nome_Time = jogo._Nome_Time_Casa
        _Times.Add(Time)


        Time = New class_Time
        Time._Nome_Time = jogo._Nome_Time_Visitante
        _Times.Add(Time)

    End Sub
End Class
