﻿Imports System.IO

Module Rodada

    Sub Main()

        Dim Rodada As String
        Dim Jogos As Array

        Try
            Rodada = "Palteras 1 Furmengo 0, Tuintians 0 Cantos 3, Palteras 4 Cantos 0, Furmengo 1 Tuintians 0, Cantos 2 Furmengo 1, Palteras 8 Tuintians 0"

            Jogos = Rodada.Split(",")

            Dim ClassificacaoTimes As New class_Colocacao

            'Salva  os jogos
            For Each JogoTexto In Jogos
                JogoTexto = JogoTexto.ToString.TrimStart

                Dim jogo As New class_Jogo
                jogo._Nome_Time_Casa = JogoTexto.ToString.Substring(0, JogoTexto.ToString.IndexOf(" "))
                jogo._Gols_Casa = JogoTexto.ToString.Substring(jogo._Nome_Time_Casa.Length + 1, 1)
                jogo._Nome_Time_Visitante = JogoTexto.ToString.Substring(JogoTexto.ToString.IndexOf(" ") + 3, (JogoTexto.ToString.LastIndexOf(" ") - (JogoTexto.ToString.IndexOf(" ") + 3)))
                jogo._Gols_Visitante = JogoTexto.ToString.Substring(JogoTexto.ToString.LastIndexOf(" ") + 1, 1)

                ClassificacaoTimes._Jogos.Add(jogo)


            Next
            'Encerra a rodada
            ClassificacaoTimes.encerrarRodada()

            'Imprime a lista de classificacao
            Dim cont As Integer = 0
            For Each pont In From posicao In ClassificacaoTimes._Ranking Order By posicao._Pontos Descending, posicao._Gols Descending
                cont += 1

                Console.WriteLine(cont.ToString + " " + pont._Nome_Time + " " + pont._Pontos.ToString + " " + pont._Vitoria.ToString + " " + pont._Empate.ToString + " " + pont._Derrota.ToString + " " + pont._Gols.ToString)

            Next

        Catch ex As Exception
            Console.WriteLine("Erro ao calcular rodada - " + ex.Message)
        End Try

        Console.ReadKey()

    End Sub

End Module
