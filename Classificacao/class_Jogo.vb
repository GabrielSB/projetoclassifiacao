﻿Public Class class_Jogo

    Private Nome_Time_Casa As String
    Public Property _Nome_Time_Casa() As String
        Get
            Return Nome_Time_Casa
        End Get
        Set(ByVal value As String)
            Nome_Time_Casa = value
        End Set
    End Property

    Private Pontos_Casa As Integer
    Public Property _Pontos_Casa() As Integer
        Get
            Return Pontos_Casa
        End Get
        Set(ByVal value As Integer)
            Pontos_Casa = value
        End Set
    End Property

    Private Gols_Casa As Integer
    Public Property _Gols_Casa() As Integer
        Get
            Return Gols_Casa
        End Get
        Set(ByVal value As Integer)
            Gols_Casa = value
        End Set
    End Property

    Private Vitoria_Casa As Integer
    Public Property _Vitoria_Casa() As Integer
        Get
            Return Vitoria_Casa
        End Get
        Set(ByVal value As Integer)
            Vitoria_Casa = value
        End Set
    End Property

    Private Derrota_Casa As Integer
    Public Property _Derrota_Casa() As Integer
        Get
            Return Derrota_Casa
        End Get
        Set(ByVal value As Integer)
            Derrota_Casa = value
        End Set
    End Property

    Private Empate_Casa As Integer
    Public Property _Empate_Casa() As Integer
        Get
            Return Empate_Casa
        End Get
        Set(ByVal value As Integer)
            Empate_Casa = value
        End Set
    End Property

    Private Nome_Time_Visitante As String
    Public Property _Nome_Time_Visitante() As String
        Get
            Return Nome_Time_Visitante
        End Get
        Set(ByVal value As String)
            Nome_Time_Visitante = value
        End Set
    End Property

    Private Gols_Visitante As Integer
    Public Property _Gols_Visitante() As Integer
        Get
            Return Gols_Visitante
        End Get
        Set(ByVal value As Integer)
            Gols_Visitante = value
        End Set
    End Property

    Private Vitoria_Visitante As Integer
    Public Property _Vitoria_Visitante() As Integer
        Get
            Return Vitoria_Visitante
        End Get
        Set(ByVal value As Integer)
            Vitoria_Visitante = value
        End Set
    End Property


    Private Derrota_Visitante As Integer
    Public Property _Derrota_Visitante() As Integer
        Get
            Return Derrota_Visitante
        End Get
        Set(ByVal value As Integer)
            Derrota_Visitante = value
        End Set
    End Property

    Private Empate_Visitante As Integer
    Public Property _Empate_Visitante() As Integer
        Get
            Return Empate_Visitante
        End Get
        Set(ByVal value As Integer)
            Empate_Visitante = value
        End Set
    End Property

    Private Pontos_Visitante As Integer
    Public Property _Pontos_Visitante() As Integer
        Get
            Return Pontos_Visitante
        End Get
        Set(ByVal value As Integer)
            Pontos_Visitante = value

        End Set
    End Property


End Class

