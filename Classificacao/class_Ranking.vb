﻿Public Class class_Ranking


    Private Nome_Time As String
    Public Property _Nome_Time() As String
        Get
            Return Nome_Time
        End Get
        Set(ByVal value As String)
            Nome_Time = value
        End Set
    End Property

    Private Pontos As Integer
    Public Property _Pontos() As Integer
        Get
            Return Pontos
        End Get
        Set(ByVal value As Integer)
            Pontos = value
        End Set
    End Property

    Private Gols As Integer
    Public Property _Gols() As Integer
        Get
            Return Gols
        End Get
        Set(ByVal value As Integer)
            Gols = value
        End Set
    End Property

    Private Vitoria As Integer
    Public Property _Vitoria() As Integer
        Get
            Return Vitoria
        End Get
        Set(ByVal value As Integer)
            Vitoria = value
        End Set
    End Property

    Private Derrota As Integer
    Public Property _Derrota() As Integer
        Get
            Return Derrota
        End Get
        Set(ByVal value As Integer)
            Derrota = value
        End Set
    End Property

    Private Empate As Integer
    Public Property _Empate() As Integer
        Get
            Return Empate
        End Get
        Set(ByVal value As Integer)
            Empate = value
        End Set
    End Property



End Class
